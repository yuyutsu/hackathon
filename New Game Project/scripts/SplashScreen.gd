extends Control

var _progress_tween: SceneTreeTween
onready var _progress_bar: ProgressBar = $VBoxContainer/ProgressBar

# Called when the node enters the scene tree for the first time.
func _ready():
	_kill_tween()
	_progress_tween = get_tree().create_tween()
	_progress_tween.tween_property(_progress_bar, "value", 100, 4)


func _kill_tween():
	if is_instance_valid(_progress_tween):
		_progress_tween.stop()
		_progress_tween.kill()


func _on_ProgressBar_value_changed(value):
	if value >= 100:
		get_tree().change_scene("res://scene/story.tscn")
