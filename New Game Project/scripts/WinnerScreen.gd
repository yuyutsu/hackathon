extends Control


var map_texture = {
	0:"res://assets/lizard.png",
	1:"res://assets/paper.png",
	2:"res://assets/scirrsors.png",
	3:"res://assets/rock.png",
	4:"res://assets/spock.png"
}

var string_map = {
	0: "Lizard",
	1: "Paper",
	2: "Scissor",
	3: "Rock",
	4 : "Spock"
}

onready var player_texture = $MarginContainer/HBoxContainer/LEFT/VBoxContainer/TextureRect
onready var bot_texture =$MarginContainer/HBoxContainer/LEFT3/VBoxContainer/TextureRect2
onready var label = $MarginContainer/HBoxContainer/LEFT2/VBoxContainer/Label

onready var player_label = $MarginContainer/HBoxContainer/LEFT/VBoxContainer/Label2
onready var bot_label = $MarginContainer/HBoxContainer/LEFT3/VBoxContainer/Label2
var texture_map = {}


func initialize(player_input, bot_input, is_player_won, is_same = false) :
	self.show()
	player_texture.texture = load(map_texture[player_input])
	bot_texture.texture = load(map_texture[bot_input])
	player_label.text = string_map[player_input]
	bot_label.text = string_map[bot_input]
	
	if is_same:
		label.text = "Its a tie."
		return
	
	if is_player_won:
		label.text = "You won"
	else:
		label.text = "You Died"


func _on_TextureButton_pressed():
	get_tree().reload_current_scene()


func _on_Button_pressed():
	get_tree().reload_current_scene()

