extends Control

enum options {
	lizard = 0,
	paper,
	scissors,
	rock,
	spock
}

var string_map = {
	options.lizard: "lizard",
	options.paper: "paper",
	options.scissors: "scissors",
	options.rock: "rock",
	options.spock : "spock"
}

var win_data =  {
	options.lizard: [options.spock, options.paper],
	options.paper: [options.spock, options.rock],
	options.scissors: [options.lizard, options.paper],
	options.rock: [options.lizard, options.scissors],
	options.spock: [options.scissors, options.rock]
}


onready var start_button = $StartButton

onready var timer = $Timer
onready var label = $Label

var timer_count = 0


func _ready() -> void:
	start_button.show()
	$Rock.hide()
	$Paper.hide()
	$Scissors.hide()
	$Spock.hide()
	$Lizard.hide()


func _on_Timer_timeout():
	timer_count += 1
	if timer_count == 1:
		label.text = "Apna"
	
	elif timer_count == 2:
		label.text = "Haath"
	
	elif timer_count == 3:
		label.text = "Jagannath"
		
	elif timer_count > 3:
		label.text = "Select"
		$Rock.show()
		$Paper.show()
		$Scissors.show()
		$Spock.show()
		$Lizard.show()


func _on_RockTextureButton_pressed():
	_on_button_pressed(options.rock)


func _on_PaperTextureButton_pressed():
	_on_button_pressed(options.paper)


func _on_ScissorsTextureButton_pressed():
	_on_button_pressed(options.scissors)


func _on_SpockTextureButton_pressed():
	_on_button_pressed(options.spock)


func _on_LizardTextureButton_pressed():
	_on_button_pressed(options.lizard)


onready var winner_screen = $WinnerScreen

func _on_button_pressed(user_pressed_id):
	if user_pressed_id == -1:
		return

	randomize()

	var computer_option = randi()%5
	print("computer chose: ", string_map[computer_option])
	if user_pressed_id == computer_option:
		winner_screen.initialize(user_pressed_id, computer_option, false, true)
		return

	var player_winnings = win_data[user_pressed_id]
	if player_winnings.has(computer_option):
		winner_screen.initialize(user_pressed_id, computer_option, true)
	else:
		winner_screen.initialize(user_pressed_id, computer_option, false)


func _on_StartButton_pressed():
	timer.start()
	start_button.hide()


func _on_StartButton_button_down():
	start_button.modulate.a = 0.5


func _hand_button_down():
	pass


func _on_RockTextureButton_button_down(extra_arg_0):
	pass # Replace with function body.
