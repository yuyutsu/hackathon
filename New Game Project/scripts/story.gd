extends Control

onready var label = $Label

func _ready():
	label.text = "You're Walking along a dark alley, alone at night."

var array = ["Suddenly, a dark figure appears. It is the Devil. ","He presents you with a challenge. ","You have to chosse one among five options.","The devil will choose his own option.","If you win, you get to leave unharmed, otherwise you die.", "Otherwise your death is imminent."]

var index: int = 0

func _on_Button_pressed():
	if index == array.size() - 1:
		get_tree().change_scene("res://scene/Main.tscn")
	label.text = array[index]
	index += 1
